# Theory and local optima of nonconvex high-dimensional M-estimators
Po-Ling Loh

In this short course, we will survey a variety of recent results concerning
local optima of penalized M-estimators, designed for high-dimensional regression
problems. The nonconvexity is allowed to arise in either the loss function or
the regularizer. Although the overall landscape of the objective function is
nonconvex in high dimensions, we are able to establish that both local and
global optima are statistically consistent under appropriate conditions. Our
theory is applicable to settings involving errors-in-variables models and other
contaminated data scenarios. In the second part of the course, we will discuss
statistical and optimization theory for nonconvex M-estimators suited for robust
regression in high dimensions.

## NOTES

__M-estimators__

- Definition 1: extremum estimators for which the objective function is a sample
  average e.g. non-linear least squares and maximum likelihood estimator.
- Definition 2: zero of an estimating function (often a derivative of another
  statistical function, for example, in maximum likelihood estimator).

__errors-in-variable models__
allows measurement errors in the independent variables.

    x = x* + η
    y = y* + ε
    y* = g(x*, w|Θ)

where w are the error free regressors.

__Probably good references (freely available)__:

1. https://arxiv.org/pdf/1803.04362.pdf
2. (**\***) http://www.jmlr.org/papers/volume16/loh15a/loh15a.pdf (This is from
   the instructor).
3. A shorter version is here: https://papers.nips.cc/paper/4904-regularized-m-estimators-with-nonconvexity-statistical-and-algorithmic-theory


 
# Chemical learning logic in biological neural networks
Upinder Bhalla, NCBS

Biological systems are very efficient at learning by example and by
reinforcement. An organism typically only has a few trials to learn about
environmental associations before it starves or is eaten. These learning rules
are further constrained by the architecture of the nervous system, in which
plausible learning rules are local to each connection (synapse) and backward
propagation of information is excluded. We have been working to systematically
catalogue what is experimentally known about the natural rules for learning, and
to model these using mechanistically detailed signaling pathways. In the brain,
these learning rules are implemented through networks of chemical signalling
pathways, which we model through systems of differential equations. I will show
how our detailed chemical network model works to account for a wide range of
experiments which we have structured into a database for automated model testing
and optimization. I propose that these signaling networks implement learning
rules that are a good reference point for analyzing biological neural network
learning and may have implications for machine learning calculations.  

# Concentration results for stochastic approximation
Vivek Borkar, IIT-Bombay 

This talk will summarise a recent result on concentration of a stochastic
approximation iteration around a limiting o.d.e. trajectory and some further
variants.

# NOTES

- Computationally attractive abstraction.
- Handles noise naturally.
- Online adaptive control? 

x_{n+1} = x_n + ɑ(n)[h(x_n)+M_{n+1}]
where,
    x_n : input to system,
    M_n : noise (uncorrelated with the past)
    [h(x_n)+M_{n+1}] : measurements.

Robbins and Monro: Σa(n)=∞. Σa(n)²<∞ 

See the [code here](./code/stoch_ode.py)

![Result.](./code/stoch_ode.py.png)
 
# Estimating low-rank matrices in noise: phase transitions from spin glass theory
Yash Deshpande, MIT

Estimating low-rank matrices from noisy observations is a common task in
statistical and engineering applications. Following the seminal work of
Johnstone, Baik, Ben-Arous and Peche, versions of this problem have been
extensively studied using random matrix theory. In this talk, we will consider
an alternative viewpoint based on tools from mean field spin glasses. We will
present two examples that illustrate how these tools yield information beyond
those from classical random matrix theory. The first example is the two-groups
stochastic block model (SBM), where we will obtain a full information-theoretic
understanding of the estimation phase transition. In the second example, we will
augment the SBM with covariate information at nodes, and obtain results on the
altered phase transition.  This is based on joint works with Emmanuel Abbe,
Andrea Montanari, Elchanan Mossel and Subhabrata Sen
 
# Hard Thresholding based methods for Robust Learning 
Prateek Jain, Microsoft

Learning in presence of outliers is a critical problem that can heavily affect
performance of the learning algorithms in practice. In this talk, we present a
general approach for learning with outliers, where we iteratively estimate the
model parameters with estimated inliers and threshold out point which seems
unlikely to be generated from the model to obtain more refined set of inliers.
We instantiate this general approach for the outlier efficient PCA problem and
demonstrate that it leads to nearly optimal solution in O(PCA) computation time.
 
# Bridging the inequality gap 
Varun Jog, Wisconsin-Madison

Reconstructing probability distributions from projections is a fundamental
problem in many engineering applications, including compressed sensing, CT/PET
imaging, and seismology. Geometric and information theoretic inequalities
provide important mathematical tools for understanding the behavior of such
projections--in particular, characterizing extremal distributions with respect
to different lower-dimensional properties of interest. This talk will consist of
two parts: First, we introduce new methods to bound volumes and surface areas of
unseen geometric objects using information derived from lower-dimensional
projections. Second, we explore inequalities relating the entropies of a
distribution and its lower-dimensional projections. The common theme is a
powerful family of functional inequalities known as the Brascamp-Lieb
Inequalities. Building upon the work of Carlen & [UTF-8?]Corderoâ€“Erausquin
(2008) and Geng & Nair (2014), we develop a superadditivity result for a new
notion of Fisher information and a novel proof strategy for proving an
inequality that unifies the Brascamp-Lieb Inequalities and the Entropy Power
Inequality.  Information theoretic perspectives on learning algorithms In
statistical learning theory, generalization error is used to quantify the degree
to which a supervised machine learning algorithm may overfit to training data.
We overview some recent work [Xu and Raginsky (2017)] that bounds generalization
error of empirical risk minimization based on the mutual information I(S;W)
between the algorithm input S and the algorithm output W. We leverage these
results to derive generalization error bounds for a broad class of iterative
algorithms that are characterized by bounded, noisy updates with Markovian
structure, such as stochastic gradient Langevin dynamics (SGLD). We describe
certain shortcomings of mutual information-based bounds, and propose alternate
bounds that employ the Wasserstein metric from optimal transport theory. We
compare the Wasserstein metric-based bounds with the mutual information-based
bounds and show that for a class of data generating distributions, the former
leads to stronger bounds on the generalization error
 
Sandeep Juneja, TIFR
Sample complexity of partition identification using multi-armed bandits  (jointly with Subhashini Krishnasamy) 
Given a vector of probability distributions, or arms, each of which can be sampled independently, we consider the problem of identifying the partition to which this vector belongs from a finitely partitioned universe of such vector of distributions. We study this as a pure exploration problem in multi armed bandit settings and develop sample complexity bounds on the total mean number of samples required for identifying the correct partition with high probability. This framework subsumes well studied problems in the literature such as finding the best arm or the best few arms. We consider distributions belonging to the single parameter exponential family and primarily consider partitions where the vector of means of arms lie either in a given set or its complement. The sets considered correspond to distributions where there exists a mean above a specified threshold, where the set is a half space and where either the set or its complement is convex. This includes polytopes and their complements. In all these settings, we characterize the lower bounds on mean number of samples for each arm. Further, we propose algorithms that can match these bounds asymptotically with decreasing probability of error. 
 
# Theory for local optima of nonconvex high-dimensional M-estimators
Po-Ling Loh, Wisconsin-Madison

In this short course, we will survey a variety of recent results concerning
local optima of penalized M-estimators, designed for high-dimensional regression
problems. The nonconvexity is allowed to arise in either the loss function or
the regularizer. Although the overall landscape of the objective function is
non-convex in high dimensions, we are able to establish that both local and
global optima are statistically consistent under appropriate conditions. Our
theory is applicable to settings involving errors-in-variables models and other
contaminated data scenarios. In the second part of the course, we will discuss
statistical and optimization theory for nonconvex M-estimators suited for robust
regression in high dimensions.
 
# Inference , Learning and Approximation.
Sanjoy Mitter, MIT

In this talk I discuss the similarities and distinctions between the problems of
Approximation, Inference and Learning Karthyek Murthy, SUTD Learning with
Distributionally Robust Optimization based on Optimal Transport Distances The
objective of this talk is to showcase optimal transport distances (which include
the well-known Wasserstein distances as a special case) as an effective tool
towards improving robustness and generalization while using empirical risk as a
vehicle to minimize population risk. Unlike empirical risk minimization which
identifies the “best” parameter choice for the given data, here we propose to
look for parameter choices that perform uniformly good for any choice of
probability distribution that is within a fixed radius (quantified by a suitable
optimal transport distance)from the empirical distribution representing the
data. Such a “distributionally robust approach” is known to recover widely
employed regularization based schemes in special cases. We extend the
applicability by showing that, for a large class of useful models, the proposed
distributionally robust formulations can be solved at least as fast, or even
faster than the empirical risk minimization problem. We also discuss the
asymptotic normality and robustness properties of the resulting estimators.
 
# Non-convex optimization and multiuser information theory
Chandra Nair, CUHK

Capacity regions in multiuser information theory were traditionally established
using an achievability argument and converses to corresponding rate regions.
Recently, however, capacity regions have been established, for non-trivial and
important classes of channels (such as additive Gaussian noise models and
others), by optimizing functionals on probability spaces generated from inner or
outer bounds and showing that the bounds match for these channels. Optimization
ideas have also been used to determine if certain achievable regions proposed in
the literature are optimal or sub-optimal in general. The study of these
non-convex optimization problems required developing new insights and
techniques. In this talk, I will outline the ideas involved in obtaining the
results, as well as illustrate the relationship of these problems to
hypercontractivity and related notions studied primarily outside information
theory. I will present some unifying observations across the family of
optimization problems that we have managed to solve, and state some elementary
instances of similar problems whose solutions would be of immense interest. 


 
# Fitting a putative manifold to noisy data. 
Hariharan Narayanan, TIFR

We give a solution to the following question from manifold learning: Suppose
data belonging to a high dimensional Euclidean space is drawn independently,
identically distributed from a measure supported on a low dimensional twice
differentiable embedded compact manifold M, and is corrupted by a small amount
of i.i.d gaussian noise. How can we produce a manifold M'; whose Hausdorff
distance to M is small and whose reach (normal injectivity radius) is not much
smaller than the reach of M? 

This is joint work with Charles Fefferman, Sergei Ivanov, Yaroslav Kurylev, and
Matti Lassas.
 
# Smoothed Analysis of Low Rank Solutions to Semidefinite Programs via Burer Monteiro Factorization 
Praneeth Netrapalli, Microsoft Research 

Semidefinite programs (SDP) are important in learning and combinatorial
optimization with numerous applications. In pursuit of low-rank solutions and
low complexity algorithms, we consider the Burer--Monteiro factorization
approach for solving SDPs. We show that all approximate local optima are global
optima for the penalty formulation of appropriately rank-constrained SDPs as
long as the number of constraints scales sub-quadratically with the desired rank
of the optimal solution. Our result is based on a simple penalty function
formulation of the rank-constrained SDP along with a smoothed analysis to avoid
worst-case cost matrices. 
 
# Training Deep Models for better Generalization, Calibration, and  Adaptation. 
Sunita Sarawagi, IIT-Bombay

Training deep neural networks entails huge investment of labeled data,  compute
power, and human effort.  We can maximize our return on this investment, by
expanding the breadth of usage of a trained deep model.  We discuss three ways
of achieving this goal.  First, we show how to train the model to generalize to
new domains without labeled/unlabeled data in that domain.  Here we exploit the
natural availability of multi-domain training data to induce a continuous
representation of domains, and interpolate in this space to hallucinate new
domains during training.  Next, we show how to enhance interoperability with
humans and other models by training well-calibrated models.  We present a new
kernel embeddings inspired trainable calibration loss that is significantly more
effective than pure likelihood-based training in outputting sound probabilities
with predictions.   Finally,  we show how to adapt the model to online feedback
at deployment time. We combine a memory-based architecture with known ideas from
boosting and online kernel learning to adaptively harness online labeled data in
a globally trained neural network. 
 
# Learning How Neural Systems Govern Behavior: Motor Control, Decision-making, and Epilepsy
Sridevi Sarma, Johns Hopkins

In this tutorial, we will demonstrate how control and estimation theory can be
applied to understand how activity in various brain regions govern behavior. We
will first describe how control theory can be used to describe performance
limitations in sensorimotor control. In particular, we will derive tradeoffs
between neural computing and accuracy in tracking fast movements (e.g. a lion
chasing a gazelle). Then, we will describe how state-space modeling can capture
variability in human behaviors during financial decision making (i.e.,
gambling), which can then be related back to neural activity. Here, we explain
why some humans display the "gambler's fallacy" belief (they think they will
lose due to a streak of recent wins despite odds of winning remaining the same)
and why other display the "hot-handedness" belief (they think they will win due
to a streak of recent wins despite odds of winning remaining the same) by
examining activity in the left and right hemispheres of the brain. Finally, we
will conclude with how stability theory of dynamical networks systems applied to
EEG networks can robustly identify where seizures start in the brain, which can
inform clinicians on how to treat the most severe epilepsy patients. 
 
# Enterprise AI: Automation, Amplification, and Managing Complexity
Gautam Shroff, TCS

In this talk I shall describe in how TCS Research is applying AI techniques in
traditional enterprises across verticals, not only for automation but also
amplification of human tasks.  For example, I shall describe our experience with
deploying deep-learning based knowledge assistants at scale within TCS. Next I
shall give some examples of how AI and IOT are transforming manufacturing, by
using deep-learning for detecting, diagnosing and sometimes predicting faults,
as well as how deep-reinforcement learning can be used to manage supply chains
from source to shelf, trade in inefficient markets, or even negotiate contracts.
Finally I shall explain two challenges to the scaling of AI within traditional
enterprises: embracing the culture of field experimentation, and figuring out
how to marry mechanisms for developing and managing complex IT systems with the
AL/ML paradigm.
 
# Zeros of polynomials and estimation in graphical models 
Piyush Srivastava, TIFR

Markov chain Monte Carlo and belief propagation are two of the main techniques
for estimation in graphical models.  Both of these are related to a rather
probabilistic paradigm of studying phase transitions in statistical mechanics,
i.e., that of decay of long-range correlations.  Another classical (and perhaps
more familiar and intuitive) paradigm of phase transitions is in terms of
non-smooth behavior of observable quantities.  Through the classical work of Lee
and Yang, this notion connects with the study of *complex* roots of "partition
functions" of graphical models.  This talk surveys some recent work, starting
with an idea of Alexander Barvinok, which leverages this latter theory to
estimate the partition function algorithmically.  Surprisingly, one currently
has examples of graphical models (e.g. the Ising model with non-zero field)
where this is the only known method for deterministic approximation of the
partition function.  This talk will be survey this curious method and perhaps
serve as an exhortation for a closer look at it.  Part of this talk is based on
joint work with Jingcheng Liu and Alistair Sinclair, and with Nicholas J. A.
Harvey and Jan Vondrák, while the rest is a survey of recent work by several
authors.
 
# An Information-Geometric Framework for Learning in High Dimensions 
Gregory Wornell, MIT

We describe a framework for addressing the problem of data feature selection
prior to inference task specification, which is central to high-dimensional
learning. Introducing natural notions of universality for such problems, we
develop a local equivalence among them. We further express the information
geometry associated with this analysis, which represents a conceptually and
computationally useful learning methodology. The development will highlight the
interrelated roles of the singular value decomposition,
Hirschfeld-Gebelein-Renyi maximal correlation, canonical correlation and
principle component analyses, Tishby's information bottleneck, Wyner's common
information, Ky Fan k-norms, and Brieman and Friedman's alternating conditional
expectation algorithm. As will be discussed, this framework provides a basis for
understanding and optimizing aspects of learning systems, including neural
network architectures, matrix factorization methods for collaborative filtering,
rank-constrained multivariate linear regression, and semi-supervised learning,
among others.

# Online Learning for Structured Loss Spaces 
Siddharth Barman, IISc

In this talk we will consider prediction with expert advice when the  loss
vectors are assumed to lie in a set described by the sum of atomic  norm balls.
We will discuss regret bounds for a general version of the  online mirror
descent (OMD) algorithm that uses a combination of  regularizers, each adapted
to the constituent atomic norms. This general  result recovers standard OMD
regret bounds, and yields regret bounds for  new structured settings where the
loss vectors are (i) noisy versions of  points from a low-rank subspace, (ii)
sparse vectors corrupted with  noise, and (iii) sparse perturbations of low-rank
vectors.

## NOTES

Above is from this paper: https://arxiv.org/abs/1706.04125

- Related to online convex optimization. Possibly have connection with
  sequential optimization (if such a term exists).
- 


