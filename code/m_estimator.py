"""m_estimator.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import statsmodels.api as sm

def main():
    print( '[INFO] See https://www.statsmodels.org/dev/examples/notebooks/generated/robust_models_1.html')
    x = np.array( [1,2,3,4,500] )
    print( x )
    print( 'The mean is not a robust estimator of location: ', end = ' ')
    print(x.mean())
    p = sm.robust.scale.mad(x)
    print( p )

if __name__ == '__main__':
    main()

