"""stoch_ode.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import math
import random
import matplotlib.pyplot as plt
import numpy as np

def h(x):
    return math.sin(x)

def system1(y0, t, dt):
    return  y0 + dt * h(t)

def system2(y0, t, dt):
    n = random.gauss(0, 0.5)
    y = y0 + dt * ( h(t) + n )
    return y

def deterministic(y0, N = 3000):
    ts, ys = [], []
    t, dt = 0, 0.01
    for i in range(N):
        ts.append(t)
        ys.append(y0)
        y0 = system1(y0, t, dt)
        t += dt
    return ts, ys

def stochastic(y0, N = 1000000):
    ts, ys = [], []
    t, dt = 0, 0.01
    for i in range(N):
        # scale the dt
        dt = dt * 0.999
        ts.append(t)
        ys.append(y0)
        y0 = system2(y0, t, dt)
        t += dt
    return ts, ys

def main():
    t1, y1 = deterministic(0.1)
    t2, y2 = stochastic(0.1)
    plt.plot(t1, y1, alpha=0.8, label = 'determinstic')
    plt.plot(t2, y2, lw=2, alpha=0.8, label = 'stochastic')
    plt.legend()
    plt.savefig( '%s.png' % __file__ )

if __name__ == '__main__':
    main()

